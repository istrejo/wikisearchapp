import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { AppComponent } from './app.component';
import { SearchModule } from './pages/search/search.module';
import { ArticleModule } from './pages/article/article.module';
import { SearchService } from './pages/search/services/search.service';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, SearchModule, HttpClientModule, ArticleModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
