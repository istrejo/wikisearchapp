import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, tap, filter, debounceTime, distinctUntilChanged } from 'rxjs';

@Component({
  selector: 'app-search',
  template: `
    <div class="form">
      <form action="">
        <div class="form field">
          <input type="text" [formControl]="inputSearch" placeholder="Search" />
        </div>
      </form>
    </div>
  `,
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  inputSearch = new FormControl('');
  @Output() submitted = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {
    this.onChange();
  }

  onChange(): void {
    this.inputSearch.valueChanges
      .pipe(
        map((search: string) => search.trim()),
        debounceTime(300),
        distinctUntilChanged(),
        filter((search: string) => search !== ''),
        tap((search: string) => this.submitted.emit(search))
      )
      .subscribe();
    // this.inputSearch.valueChanges
    //   .pipe(tap((res) => this.submitted.emit(res)))
    //   .subscribe();
  }
}
